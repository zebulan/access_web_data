from bs4 import BeautifulSoup
from requests import get

URL = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/' \
      'comments_42.html'
URL2 = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/' \
       'comments_178365.html'


def get_result(url):
    r = get(url)
    soup = BeautifulSoup(r.text)
    return sum(int(a.text) for a in soup.find_all('span'))

assert get_result(URL) == 2553
assert get_result(URL2) == 2502
