from requests import get

URL = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/comments_42.json'
URL2 = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/' \
       'comments_178366.json'


def get_result(url):
    return sum(a['count'] for a in get(url).json()['comments'])

assert get_result(URL) == 2553
assert get_result(URL2) == 2387
