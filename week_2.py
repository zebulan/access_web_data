from re import findall


def get_result(filename):
    with open(filename, 'r') as f:
        return sum(map(int, findall(r'\d+', f.read())))

assert get_result('w2_test.txt') == 445822
assert get_result('w2_test_2.txt') == 511872
