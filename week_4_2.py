from bs4 import BeautifulSoup
from requests import get

URL = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/' \
      'known_by_Fikret.html'
URL2 = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/' \
       'known_by_Wang.html'


def get_result(url, link_pos, repeat):
    for a in range(1, repeat + 1):
        r = get(url)
        soup = BeautifulSoup(r.text)
        if not a == repeat:
            url = soup.find_all('a')[link_pos - 1]['href']
        else:
            return soup.find_all('a')[link_pos - 1].text

assert get_result(URL, 3, 4) == 'Anayah'
assert get_result(URL2, 18, 7) == 'Romey'
