from requests import get

GOOGLE = 'http://maps.googleapis.com/maps/api/geocode/json'
DR_CHUCK = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/geojson'


def get_result(url, address):
    payload = {
        'address': address,
        'sensor': 'false'
    }
    return get(url, payload).json()['results'][0]['place_id']

assert get_result(GOOGLE, 'Universidad de Valladolid') \
    == 'ChIJQ3voe68SRw0RMsmQOvIw8cg'
assert get_result(DR_CHUCK, 'Universidad de Valladolid') \
    == 'ChIJQ3voe68SRw0RMsmQOvIw8cg'
