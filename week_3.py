from requests import get


def get_result(url):
    for k, v in sorted(get(url).headers.items()):
        print('{}: {}\n'.format(k, v))

get_result('http://www.pythonlearn.com/code/intro-short.txt')
