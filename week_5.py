from bs4 import BeautifulSoup
from requests import get

URL = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/comments_42.xml'
URL2 = 'http://pr4e.dr-chuck.com/tsugi/mod/python-data/data/' \
       'comments_178362.xml'


def get_result(url):
    r = get(url)
    soup = BeautifulSoup(r.text, 'lxml')
    return sum(int(a.text) for a in soup.find_all('count'))

assert get_result(URL) == 2553
assert get_result(URL2) == 2352
