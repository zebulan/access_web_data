#Using Python to Access Web Data

My solutions are written in `Python 3` and are using `Requests` and `Beautiful Soup 4` (where applicable).

[Using Python to Access Web Data (Coursera)](https://www.coursera.org/learn/python-network-data)
